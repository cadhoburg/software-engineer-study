# Software Engineer study

## Uitleg

De opdrachten die ik vanuit de HAN moet uitvoeren. Deze worden gecontroleerd en aangepast.

### Prerequisits

Voor module 1 en 2 is het programma Processing nodig. Deze kun je hier downloaden:

[Download link] (https://processing.org/download/)

Bij opdracht 2 moet je gebruik maken van een Processing library. Dit kun je installeren op de volgende manier:

1. Open processing
2. Selecteer in het menu Sketch -> Import library -> Add Library
3. Zoek in het scherm naar ControlP5
4. Selecteer ControlP5 en klik op "Install"

