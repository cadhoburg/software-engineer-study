import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Main extends PApplet {

final int ACHTERGRONDKLEUR = (0xff4B4A4A);
final int WIT = (0xffFFFFFF);
final int KAARTKLEUR = (0xffF0F0F0);
final int Y_POSITIE_VAN_LIJN_STATUSSCHERM = 610;
final int STATUSSCHERM_LIJNDIKTE = 4;
final int GROOTTE_KAART = 200;
final int BLAUW = 0xff3F42DE;
final int ROOD = 0xffF70C0C;
final int EINDSPEL_INFO_KLEUR = 0xff890F0F;
final int GROEN = 0xff34CE35;
final int OFFSET = 2;
final int SELECTIEKLEUR = 0xff426471;
final String[] AANTAL = {"1 ", "2 ", "3 "};
final String[] KLEUREN = {"r ", "g ", "b "};
final String[] FIGUREN = {"d", "r", "e"};

String[] kaarten = kaartGenerator(AANTAL, KLEUREN, FIGUREN);
String[] kaartenOpScherm = kaartenOpScherm(kaarten, 9);
String[] geklikteKaarten = new String [3];
int[] indexNummers = new int [3];

int teller = 0;
int score = 0;
int strafpunten = 0;

boolean spelKlaar =  false;

/* 
Dit programma zal bij het opstarten 9 kaarten tonen. Dit is een replica (simpelere vorm) van het kaartspel SET.
Een speler kan 3 kaarten kiezen. Is het een set, dan krijgt de speler een punt erbij. Is het geen set, dan krijgt de speler een strafpunt.
Uiteindelijk is de score - de strafpunten de definitieve score.
*/

public void setup() {
  
  background(ACHTERGRONDKLEUR);
  stroke(WIT);
  line(0, Y_POSITIE_VAN_LIJN_STATUSSCHERM, width, Y_POSITIE_VAN_LIJN_STATUSSCHERM);
  tekenKaartenOpScherm(kaartenOpScherm);
  tekenInfoVeld(score, strafpunten);
}

public void draw() {

}
public void mouseClicked() {

  int kaartOpIndex =  selecteerKaartOpCoordinaat(mouseX, mouseY);
  String nieuweKaart = kaartenOpScherm[kaartOpIndex];
  
  if (kaarten.length != 0) {
    tekenKaart(nieuweKaart, kaartOpIndex, true);
  }
  
  if (teller == 0) {
    geklikteKaarten[0] = nieuweKaart;
    indexNummers[0] = kaartOpIndex;

  }
  else if (teller == 1) {
    geklikteKaarten[1] = nieuweKaart;
    indexNummers[1] = kaartOpIndex;
  }
  else {
    geklikteKaarten[2] = nieuweKaart;
    indexNummers[2] = kaartOpIndex;
  }
  teller++;
  
  if (geklikteKaarten[0] == geklikteKaarten[1]) {
    tekenKaart(nieuweKaart, kaartOpIndex, false);
    geklikteKaarten[0] = null;
    geklikteKaarten[1] = null;
    teller = 0;
  }
  
  if (teller >= 2) {
    if (geklikteKaarten[0] == geklikteKaarten[2]) {
      tekenKaart(nieuweKaart, kaartOpIndex, false);
      geklikteKaarten[2] = null;
      geklikteKaarten[0] = geklikteKaarten[1];
      geklikteKaarten[1] = null;
      teller = 1;
    }
  }
  
  if (teller >= 2) {
    if (geklikteKaarten[1] == geklikteKaarten[2]) {
    tekenKaart(nieuweKaart, kaartOpIndex, false);
    geklikteKaarten[1] = null;
    geklikteKaarten[2] = null;
    teller = 1;
    }
  }
  
  if (teller == 3) {
    if (geklikteKaarten[0] != geklikteKaarten[1]) {
    isSet(geklikteKaarten);
    }
  }
  
  if (spelKlaar) {
    if (isInJaBox()) {
      kaarten = kaartGenerator(AANTAL, KLEUREN, FIGUREN);
      kaartenOpScherm = kaartenOpScherm(kaarten, 9);
      tekenKaartenOpScherm(kaartenOpScherm);
      score = 0;
      teller = 0;
      strafpunten = 0;
      stroke(WIT);
      line(0, Y_POSITIE_VAN_LIJN_STATUSSCHERM, width, Y_POSITIE_VAN_LIJN_STATUSSCHERM);
      tekenInfoVeld(score, strafpunten);
      spelKlaar = false;
    }
    if (isInNeeBox()) {
      background(ACHTERGRONDKLEUR);
      textSize(30);
      fill(GROEN);
      text("BEDANKT VOOR HET SPELEN!", width / 4, height / 2);
    }
  }
}

/**
 * Deze methode accepteert 3 arrays met eigenschappen en geeft 1 array met 27 kaarten terug.
 */
public String[] kaartGenerator(String[] aantal, String[] kleuren, String[] figuren) {

  int totaleLengteArrays = aantal.length * kleuren.length *
    figuren.length;

  String[ ] kaarten = new String[totaleLengteArrays];

  int index = 0;

  for (int i = 0; i < aantal.length; i++) {
    for (int j = 0; j < kleuren.length; j++) {
      for (int k = 0; k < figuren.length; k++) {
        kaarten[index] = aantal[i] + kleuren[j] + figuren[k];
        index++;
      }
    }
  }
  return kaarten;
}


/**
 * De array met 27 kaarten wordt hier als argument geaccepteert.
 * Hieruit wordt er een nieuwe array met 9 willekeurige kaarten teruggegeven.
 */
public String[] kaartenOpScherm(String[] kaartenArray, int n) {

  String[] teSpelenKaarten = new String[n];

  for (int i = 0; i < teSpelenKaarten.length; i++) {

    int randomKaart = PApplet.parseInt(random(kaartenArray.length));
    teSpelenKaarten[i] = kaartenArray[randomKaart];

    String laatsteKaart = kaartenArray[kaartenArray.length - 1];
    kaartenArray[randomKaart] = laatsteKaart;

    kaartenArray = shorten(kaartenArray);
  }


  kaarten = kaartenArray;

  return teSpelenKaarten;
}

public int selecteerKaartOpCoordinaat(int xPos, int yPos) {

  int iX = xPos / GROOTTE_KAART;
  int iY = yPos / GROOTTE_KAART;
  int indexNummer = 0;

  int[] kaartCoordinaat = {iX, iY};

  if (kaartCoordinaat[0] == 0 && kaartCoordinaat[1] == 0) {
    indexNummer = 0;
  } else if (kaartCoordinaat[0] == 1 && kaartCoordinaat[1] == 0) {
    indexNummer = 1;
  } else if (kaartCoordinaat[0] == 2 && kaartCoordinaat[1] == 0) {
    indexNummer = 2;
  } else if (kaartCoordinaat[0] == 0 && kaartCoordinaat[1] == 1) {
    indexNummer = 3;
  } else if (kaartCoordinaat[0] == 1 && kaartCoordinaat[1] == 1) {
    indexNummer = 4;
  } else if (kaartCoordinaat[0] == 2 && kaartCoordinaat[1] == 1) {
    indexNummer = 5;
  } else if (kaartCoordinaat[0] == 0 && kaartCoordinaat[1] == 2) {
    indexNummer = 6;
  } else if (kaartCoordinaat[0] == 1 && kaartCoordinaat[1] == 2) {
    indexNummer = 7;
  } else if (kaartCoordinaat[0] == 2 && kaartCoordinaat[1] == 2) {
    indexNummer = 8;
  }

  return indexNummer;
}


public boolean isSet(String[] geklikteKaarten) {

  boolean isSet = false;
  boolean aantalSet = false;
  boolean kleurSet = false;
  boolean vormSet = false;


  String kaart1 = geklikteKaarten[0];
  String kaart2 = geklikteKaarten[1];
  String kaart3 = geklikteKaarten[2];


  String aantal1 = kaart1.substring(0, 1);
  String aantal2 = kaart2.substring(0, 1);
  String aantal3 = kaart3.substring(0, 1);

  String kleur1 = kaart1.substring(2, 3);
  String kleur2 = kaart2.substring(2, 3);
  String kleur3 = kaart3.substring(2, 3);

  String vorm1 = kaart1.substring(4);
  String vorm2 = kaart2.substring(4);
  String vorm3 = kaart3.substring(4);


  if ((!aantal1.equals(aantal2) && !aantal2.equals(aantal3) && !aantal1.equals(aantal3)) || 
    (aantal1.equals(aantal2) && aantal2.equals(aantal3) && aantal1.equals(aantal3))) {
    aantalSet = true;
  }

  if ((!kleur1.equals(kleur2) && !kleur2.equals(kleur3) && !kleur1.equals(kleur3)) || 
    (kleur1.equals(kleur2) && kleur2.equals(kleur3) && kleur1.equals(kleur3))) {
    kleurSet = true;
  }

  if ((!vorm1.equals(vorm2) && !vorm2.equals(vorm3) && !vorm1.equals(vorm3)) || 
    (vorm1.equals(vorm2) && vorm2.equals(vorm3) && vorm1.equals(vorm3))) {
    vormSet = true;
  }

  if (aantalSet && kleurSet && vormSet) {
    score++;
    teller -= 3;
    isSet = true;
    fill(ACHTERGRONDKLEUR);
    noStroke();
    rect(30, GROOTTE_KAART * 3 + 15, 400, 200);
    tekenDrieKaartenOpnieuw((kaartenOpScherm(kaarten, 3)), indexNummers, false);
    tekenInfoVeld(score, strafpunten);
    if (score == 6) {
      fill(ACHTERGRONDKLEUR);
      noStroke();
      rect(0, GROOTTE_KAART * 3, width, 500);
    }
  } else {
    tekenDrieKaartenOpnieuw(geklikteKaarten, indexNummers, false);
    teller -=3;
    strafpunten++;
    fill(ACHTERGRONDKLEUR);
    noStroke();
    rect(30, GROOTTE_KAART * 3 + 15, 400, 200);
    tekenInfoVeld(score, strafpunten);
    geklikteKaarten[0] = null;
    geklikteKaarten[1] = null;
    geklikteKaarten[2] = null;
  }


  return isSet;
}

public boolean isInJaBox() {
  
  boolean geklikt = false;
  if (mouseX > width / 2.5f && mouseX < width / 2.5f + 50 && mouseY > height / 2 + 60 && mouseY < height / 2 + 60 + 20) {
    geklikt = true;
  }
  return geklikt;
}

public boolean isInNeeBox() {
  
  boolean geklikt = false;
  if (mouseX > width / 2 && mouseX < width / 2 + 50 && mouseY > height / 2 + 60 && mouseY < height / 2 + 60 + 20) {
    geklikt = true;
  }
  return geklikt;
}
  
public int geeftOffset(int aantal) {
  
  if (aantal == 1) {
    return 40;
  }
  if (aantal == 2) {
    return 20;
  }
  if (aantal == 3) {
    return 0;
  }
  
  return 0;
}
  
public void tekenRechthoekKaart(int aantal, int kleur, int xPositie, int yPositie, boolean isGeselecteerd) {

  int breedteRechthoek = GROOTTE_KAART / 2;
  int hoogteRechthoek = GROOTTE_KAART / 5;
  int tussenRuimteRechthoeken = 5;
  int achtergrondKaart = WIT;

  if (isGeselecteerd) {
    achtergrondKaart = SELECTIEKLEUR;
  }
  

  tekenAchtergrond(xPositie + OFFSET, yPositie + OFFSET, GROOTTE_KAART - OFFSET * 2, GROOTTE_KAART - OFFSET * 2, achtergrondKaart);

  for (int i = 0; i < aantal; i++) {
    fill(kleur);
    rect(xPositie + GROOTTE_KAART / 4, yPositie + GROOTTE_KAART / 5 + geeftOffset(aantal), breedteRechthoek, hoogteRechthoek);
    yPositie += hoogteRechthoek + tussenRuimteRechthoeken;
  }
}


public void tekenDriehoekKaart(int aantal, int kleur, int xPositie, int yPositie, boolean isGeselecteerd) {
  int zijdeAbAc = 40;
  int zijdeBc = 30;
  int tussenRuimteDriehoeken = 10;
  int achtergrondKaart = WIT;

  if (isGeselecteerd) {
    achtergrondKaart = SELECTIEKLEUR;
  }
  int offSet = 50;
  
  if (aantal == 2) {
    offSet = 25;
  }
  else if (aantal == 3) {
    offSet = 0;
  }
  
  tekenAchtergrond(xPositie + OFFSET, yPositie + OFFSET, GROOTTE_KAART - OFFSET * 2, GROOTTE_KAART - OFFSET * 2, achtergrondKaart);

  for (int i = 0; i < aantal; i++) {
    fill(kleur);
    triangle(xPositie + GROOTTE_KAART / 2, yPositie + GROOTTE_KAART / 10 + geeftOffset(aantal), xPositie + GROOTTE_KAART / 2 + zijdeAbAc, yPositie + GROOTTE_KAART / 5 + zijdeBc + geeftOffset(aantal), 
              xPositie + GROOTTE_KAART / 2 - zijdeAbAc, yPositie + GROOTTE_KAART / 5 + zijdeBc + geeftOffset(aantal));
    yPositie += zijdeAbAc + tussenRuimteDriehoeken;
  }
}



public void tekenEllipsKaart(int aantal, int kleur, int xPositie, int yPositie, boolean isGeselecteerd) {

  int breedteEllips = GROOTTE_KAART / 2;
  int hoogteEllips = GROOTTE_KAART / 4;
  int tussenRuimteEllips = 5;
  int achtergrondKaart = WIT;

  if (isGeselecteerd) {
    achtergrondKaart = SELECTIEKLEUR;
  }


  tekenAchtergrond(xPositie + OFFSET, yPositie + OFFSET, GROOTTE_KAART - OFFSET * 2, GROOTTE_KAART - OFFSET * 2, achtergrondKaart);

  for (int i = 0; i < aantal; i++) {
    fill(kleur);
    ellipse(xPositie + GROOTTE_KAART / 2, yPositie + GROOTTE_KAART / 5 + geeftOffset(aantal), breedteEllips, hoogteEllips);
    yPositie += hoogteEllips + tussenRuimteEllips;
  }
}

public void tekenAchtergrond(int xPositie, int yPositie, int breedte, int hoogte, int achtergrondKleur) {

  fill(achtergrondKleur);
  rect(xPositie + OFFSET, yPositie + OFFSET, GROOTTE_KAART - OFFSET * 2, GROOTTE_KAART - OFFSET * 2);
}


public void tekenKaartenOpScherm(String[] kaarten) {

  for (int i = 0; i < kaarten.length; i++) {
    tekenKaart(kaarten[i], i, false);
  }
}

public void tekenKaart(String kaart, int index, boolean conditie) {
  int aantal = 0;
  int kleur = 0;
  int row = index / 3;
  int col = index % 3;
  String[] kaartGesplit = splitTokens(kaart);

  if (kaartGesplit[0].equals("1")) {
    aantal = 1;
  } else if (kaartGesplit[0].equals("2")) {
    aantal = 2;
  } else {
    aantal = 3;
  }

  if (kaartGesplit[1].equals("r")) {
    kleur = ROOD;
  } else if (kaartGesplit[1].equals("b")) {
    kleur = BLAUW;
  } else {
    kleur = GROEN;
  }

  if (kaartGesplit[2].equals("r")) {
    tekenRechthoekKaart(aantal, kleur, col * GROOTTE_KAART, row * GROOTTE_KAART, conditie);
  } else if (kaartGesplit[2].equals("d")) {
    tekenDriehoekKaart(aantal, kleur, col * GROOTTE_KAART, row * GROOTTE_KAART, conditie);
  } else {
    tekenEllipsKaart(aantal, kleur, col * GROOTTE_KAART, row * GROOTTE_KAART, conditie);
  }
}

public void tekenDrieKaartenOpnieuw(String[] kaart, int[] index, boolean conditie) {

  kaartenOpScherm[index[0]] = kaart[0];
  kaartenOpScherm[index[1]] = kaart[1];
  kaartenOpScherm[index[2]] = kaart[2];

  if (kaarten.length != 0) {
    for (int i = 0; i < 3; i++) {
      tekenKaart(kaart[i], index[i], false);
    }
  } else {
    tekenEindscherm();
  }
}


public void tekenInfoVeld(int score, int strafpunten) {

  fill(255);
  textSize(20);
  text("Aantal gevonden sets: " + score, 30, GROOTTE_KAART * 3 + 30);
  text("Jouw strafpunten zijn: " + strafpunten, 30, GROOTTE_KAART * 3 + 180);
}

public void tekenEindscherm() {

  background(0xff4B4A4A);
  fill(EINDSPEL_INFO_KLEUR);
  textSize(20);
  text("HET SPEL IS AFGELOPEN", width / 2.5f, height / 2);
  text("Uw score is: " + (score - strafpunten), width / 2.5f, height / 2 + 20);
  text("Nog een keer spelen?", width / 2.5f, height / 2 + 50);
  fill(ACHTERGRONDKLEUR);
  noStroke();
  rect(30, GROOTTE_KAART * 3 + 15, 400, 200);
  fill(GROEN);
  rect( width / 2.5f, height / 2 + 60, 50, 20);
  fill(ROOD);
  rect( width / 2, height / 2 + 60, 50, 20);
  fill(WIT);
  textSize(15);
  text("JA", width / 2.5f + 5, height / 2 + 60 + 15);
  text("NEE", width / 2 + 5, height / 2 + 60 + 15);
  spelKlaar = true;
} 
  public void settings() {  size(600, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Main" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
