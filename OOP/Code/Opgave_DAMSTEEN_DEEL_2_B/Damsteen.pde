class Damsteen {
  int x, y, kleur, diameter;
  boolean isGeselecteerd = false;

  Damsteen(int x, int y, int kleur, int diameter) {
    this.x = x;
    this.y = y;
    this.kleur = kleur;
    this.diameter = diameter;
  }

  void tekenDamsteen() {
    if (isGeselecteerd) {
      stroke(#FC141C);
      strokeWeight(2);
    } else {
      noStroke();
    }
    fill(kleur);
    circle(x, y, diameter);
  }
}
