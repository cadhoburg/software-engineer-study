Damsteen[] damstenen = new Damsteen[4];
final int marge = 20;

void setup() {
  size(400, 400);
 
  for (int i = 0; i < damstenen.length; i++) {
    if (i < 2) {
      damstenen[i] = new Damsteen(i * 20 + marge, i * 20 + marge, 255, 20);
    } else {
      damstenen[i] = new Damsteen(i * 50 + marge, i * 50 + marge, 0, 20);
    }
  }
  damstenen[0].isGeselecteerd = true;
}

void draw() {
  for (int i = 0; i < damstenen.length; i++) {
    damstenen[i].tekenDamsteen();
  }
}
